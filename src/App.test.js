import configureMockStore from "redux-mock-store";

import { initialRootState } from "./store/reducers/RootReducer";
import App from "./App";

const mockStore = configureMockStore();
const store = mockStore({ ...initialRootState });
const wrapper = providerWrapper(App, {}, store);
const appComponent = _wrapper => _wrapper.find("App");

it("renders without crashing", () => {
  expect(appComponent(wrapper).length).toEqual(1);
});
