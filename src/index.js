import React from "react";
import { Provider } from "react-redux";
import ReactDOM from "react-dom";

import Store from "./store";
import "./index.css";
import App from "./App";

ReactDOM.render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
