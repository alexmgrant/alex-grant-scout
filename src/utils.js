const isObject = item => {
  return !!item && item.constructor.name === "Object";
};

const getArrayOfObjects = object =>
  Object.entries(object).map(([key, value]) => ({
    key,
    value
  }));

export const convertObjectToArray = object => {
  if (!isObject(object)) {
    console.warn(`convertObjectToArray was passed a non object of: ${object}`);

    return object;
  }

  const arrayOfObjects = getArrayOfObjects(object);

  return arrayOfObjects;
};
