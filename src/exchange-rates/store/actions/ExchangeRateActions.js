import moment from "moment";
import { convertObjectToArray } from "../../../utils";
import { getTimestamp } from "../selectors/ExchangeRatesSelectors";

export const GET_EXCHANGE_RATE = "[Exchange Rate] Get Exchange Rates";
export const GET_EXCHANGE_RATE_SUCCESS =
  "[Exchange Rate] Get Exchange Rates Success";
export const GET_EXCHANGE_RATE_FAIL = "[Exchange Rate] Get Exchange Rates Fail";

export const GET_PREVIOUS_EXCHANGE_RATE =
  "[Exchange Rate] Get Previous Exchange Rate";
export const GET_PREVIOUS_EXCHANGE_RATE_SUCCESS =
  "[Exchange Rate] Get Previous Exchange Rate Success";
export const GET_PREVIOUS_EXCHANGE_RATE_FAIL =
  "[Exchange Rate] Get Previous Exchange Rate Fail";

export const api = "https://api.exchangeratesapi.io";
export const fetchExchangeRates = () => fetch(`${api}/latest`);
export const fetchPreviousExchangeRates = date => fetch(`${api}/${date}`);

export const getExchangeRate = () => ({ type: GET_EXCHANGE_RATE });

export const getExchangeRateSuccess = rates => ({
  type: GET_EXCHANGE_RATE_SUCCESS,
  payload: rates
});

export const getExchangeRateFail = error => ({
  type: GET_EXCHANGE_RATE_FAIL,
  payload: error.message
});

export const getPreviousExchangeRate = () => ({
  type: GET_PREVIOUS_EXCHANGE_RATE
});

export const getPreviousExchangeRateSuccess = rates => ({
  type: GET_PREVIOUS_EXCHANGE_RATE_SUCCESS,
  payload: rates
});

export const getPreviousExchangeRateFail = error => ({
  type: GET_PREVIOUS_EXCHANGE_RATE_FAIL,
  payload: error.message
});

export const getExchangeRates = () => {
  return async dispatch => {
    try {
      dispatch(getExchangeRate());
      const response = await fetchExchangeRates();
      const json = await response.json();
      const rates = convertObjectToArray(json.rates);
      const timeStamp = json.date;

      dispatch(getExchangeRateSuccess({ rates, timeStamp }));
    } catch (error) {
      dispatch(getExchangeRateFail(error));
    }
  };
};

export const getPreviousRates = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(getPreviousExchangeRate());
      const state = getState();
      const timeStamp = getTimestamp(state);
      const formatedTimeStamp = moment(timeStamp)
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      const response = await fetchPreviousExchangeRates(formatedTimeStamp);
      const json = await response.json();
      const rates = convertObjectToArray(json.rates);

      dispatch(getPreviousExchangeRateSuccess({ rates, timeStamp }));
    } catch (error) {
      dispatch(getPreviousExchangeRateFail(error));
    }
  };
};
