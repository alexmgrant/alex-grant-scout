import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";

import * as actions from "./ExchangeRateActions";
import { initialRootState } from "../../../store/reducers/RootReducer";
import { convertObjectToArray } from "../../../utils";

const mockStore = configureMockStore([thunk]);
const store = mockStore({ ...initialRootState });
const mockResp = {
  base: "EUR",
  date: "2019-11-04",
  rates: { CAD: 1.4 }
};
const mockError = "Failed to fetch";

afterEach(() => {
  fetchMock.restore();
  store.clearActions();
});

it("dispatches GET_EXCHANGE_RATE_SUCCESS when fetch is successful", () => {
  fetchMock.getOnce("https://api.exchangeratesapi.io/latest", {
    body: mockResp
  });

  const rates = convertObjectToArray(mockResp.rates);
  const timeStamp = mockResp.date;
  const payload = { rates, timeStamp };
  const expectedActions = [
    { type: actions.GET_EXCHANGE_RATE },
    {
      type: actions.GET_EXCHANGE_RATE_SUCCESS,
      payload
    }
  ];

  return store.dispatch(actions.getExchangeRates()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

it("dispatches GET_EXCHANGE_RATE_FAIL when fetch fails", () => {
  fetchMock.getOnce("https://api.exchangeratesapi.io/latest", {
    throws: new TypeError(mockError)
  });

  const expectedActions = [
    { type: actions.GET_EXCHANGE_RATE },
    { type: actions.GET_EXCHANGE_RATE_FAIL, payload: mockError }
  ];

  return store.dispatch(actions.getExchangeRates()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
