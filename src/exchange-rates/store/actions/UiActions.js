export const TOGGLE_MODAL = "[UI] Toggle Modal";

export const toggleModal = () => dispatch => dispatch({ type: TOGGLE_MODAL });
