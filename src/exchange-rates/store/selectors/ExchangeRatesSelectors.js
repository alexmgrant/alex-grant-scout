import { createSelector } from "reselect";

export const getApiState = state => state.exchangeRates.api;
export const getUiState = state => state.exchangeRates.ui;

export const getLoading = createSelector(
  getUiState,
  ui => ui.loading
);

export const getModalOpen = createSelector(
  getUiState,
  ui => ui.modalOpen
);

export const getError = createSelector(
  getApiState,
  api => api.error
);

export const getRates = createSelector(
  getApiState,
  api => api.data
);

export const getTimestamp = createSelector(
  getApiState,
  api => api.latestTimestamp
);
