import reducer, { initialState } from "./UiReducer";
import * as uiActions from "../actions/UiActions";
import * as actions from "../actions/ExchangeRateActions";

it("should return initial state", () => {
  expect(reducer(undefined, {})).toEqual({ ...initialState });
});

it("should handle GET_EXCHANGE_RATE", () => {
  expect(reducer(initialState, { type: actions.GET_EXCHANGE_RATE })).toEqual({
    loading: true,
    modalOpen: false
  });
});

it("should handle GET_EXCHANGE_RATE_SUCCESS", () => {
  expect(
    reducer(initialState, { type: actions.GET_EXCHANGE_RATE_SUCCESS })
  ).toEqual({ loading: false, modalOpen: false });
});

it("should handle TOGGLE_MODAL & GET_EXCHANGE_RATE_FAIL", () => {
  expect(reducer(initialState, { type: uiActions.TOGGLE_MODAL })).toEqual({
    loading: false,
    modalOpen: !initialState.modalOpen
  });

  expect(
    reducer(initialState, { type: actions.GET_EXCHANGE_RATE_FAIL })
  ).toEqual({
    loading: false,
    modalOpen: !initialState.modalOpen
  });
});
