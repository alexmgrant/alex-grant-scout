import { combineReducers } from "redux";

import ui from "./UiReducer";
import api from "./ExchangeRateReducer";

const exchangeRatesReducer = combineReducers({
  api,
  ui
});

export default exchangeRatesReducer;
