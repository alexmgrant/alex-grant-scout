import reducer, { initialState } from "./ExchangeRateReducer";
import {
  getExchangeRateSuccess,
  getExchangeRateFail
} from "../actions/ExchangeRateActions";

const dateSpy = jest.spyOn(global, "Date");
const mockPayload = {
  rates: [{ CAD: 1.4 }],
  timeStamp: dateSpy.mock.instances[0]
};
const mockError = { message: "error" };

it("should return initial state", () => {
  expect(reducer(undefined, {})).toEqual({ ...initialState });
});

it("should handle GET_EXCHANGE_RATE_SUCCESS", () => {
  const { rates, timeStamp } = mockPayload;

  expect(reducer({}, getExchangeRateSuccess(mockPayload))).toEqual({
    data: rates,
    latestTimeStamp: timeStamp
  });
});

it("should handle GET_EXCHANGE_RATE_FAIL", () => {
  expect(reducer({}, getExchangeRateFail(mockError))).toEqual({
    error: mockError.message
  });
});
