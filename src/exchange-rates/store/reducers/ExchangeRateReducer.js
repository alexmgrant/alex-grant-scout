import {
  GET_EXCHANGE_RATE_SUCCESS,
  GET_EXCHANGE_RATE_FAIL,
  GET_PREVIOUS_EXCHANGE_RATE_SUCCESS
} from "../actions/ExchangeRateActions";

export const initialState = {
  data: null,
  latestTimestamp: null,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_EXCHANGE_RATE_SUCCESS:
    case GET_PREVIOUS_EXCHANGE_RATE_SUCCESS: {
      const payload = action.payload;
      const data = payload.rates;
      const latestTimestamp = payload.timeStamp;

      return {
        ...state,
        data,
        latestTimestamp
      };
    }

    case GET_EXCHANGE_RATE_FAIL: {
      const error = action.payload;

      return {
        ...state,
        error
      };
    }

    default: {
      return { ...state };
    }
  }
};
