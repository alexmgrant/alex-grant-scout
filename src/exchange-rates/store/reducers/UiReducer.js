import { TOGGLE_MODAL } from "../actions/UiActions";
import {
  GET_EXCHANGE_RATE,
  GET_EXCHANGE_RATE_SUCCESS,
  GET_EXCHANGE_RATE_FAIL,
  GET_PREVIOUS_EXCHANGE_RATE,
  GET_PREVIOUS_EXCHANGE_RATE_SUCCESS,
  GET_PREVIOUS_EXCHANGE_RATE_FAIL
} from "../actions/ExchangeRateActions";

export const initialState = {
  modalOpen: false,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_EXCHANGE_RATE:
    case GET_PREVIOUS_EXCHANGE_RATE: {
      return {
        ...state,
        loading: true
      };
    }

    case GET_EXCHANGE_RATE_SUCCESS:
    case GET_PREVIOUS_EXCHANGE_RATE_SUCCESS: {
      return { ...state, loading: false };
    }

    case TOGGLE_MODAL:
    case GET_EXCHANGE_RATE_FAIL:
    case GET_PREVIOUS_EXCHANGE_RATE_FAIL: {
      return {
        ...state,
        loading: false,
        modalOpen: !state.modalOpen
      };
    }

    default: {
      return { ...state };
    }
  }
};
