import configureMockStore from "redux-mock-store";

import { initialRootState } from "../store/reducers/RootReducer";
import ExchangeRatesContainer from "./ExchangeRatesContainer";

const mockStore = configureMockStore();
const store = mockStore({ ...initialRootState });
const wrapper = providerWrapper(ExchangeRatesContainer, {}, store);
const exchangeRatesComponent = _wrapper =>
  _wrapper.find("ExchangeRatesContainer");

it("renders without crashing", () => {
  expect(exchangeRatesComponent(wrapper).length).toEqual(1);
});
