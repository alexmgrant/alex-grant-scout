import React from "react";
import { useDispatch, useSelector } from "react-redux";

import * as selectors from "./store/selectors/ExchangeRatesSelectors";
import {
  getExchangeRates,
  getPreviousRates
} from "./store/actions/ExchangeRateActions";
import { toggleModal } from "./store/actions/UiActions";
import { Button, Spinner, Modal, Table } from "../components";

const ExchangeRatesContainer = () => {
  const dispatch = useDispatch();
  const handleCloseModal = () => dispatch(toggleModal());
  const handleGetExchangeRates = () => dispatch(getExchangeRates());
  const handleGetPreviousExchangeRates = () => dispatch(getPreviousRates());
  const loading = useSelector(selectors.getLoading);
  const modalOpen = useSelector(selectors.getModalOpen);
  const modalMessage = useSelector(selectors.getError);
  const rates = useSelector(selectors.getRates);
  const tableHeaders = ["Currency", "Rate"];

  return (
    <div>
      <Table items={rates} tableHeaders={tableHeaders} />
      <Spinner visible={loading} />
      <Button onClick={handleGetExchangeRates}>
        View the latest exchange rates
      </Button>
      <Button onClick={handleGetPreviousExchangeRates}>
        Get previous date's exchange rates
      </Button>
      <Modal
        visible={modalOpen}
        title="There was an Error"
        onClick={handleCloseModal}
        message={modalMessage}
      />
    </div>
  );
};

export default ExchangeRatesContainer;
