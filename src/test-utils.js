import React, { createElement } from "react";
import { Provider } from "react-redux";
import { mount } from "enzyme";

export const providerWrapper = (Component, initialProps = {}, store = {}) => {
  const wrapper = mount(
    createElement(
      props => (
        <Provider store={store}>
          <Component {...props} />
        </Provider>
      ),
      initialProps
    )
  );

  return wrapper;
};
