import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { providerWrapper } from "./test-utils";

configure({ adapter: new Adapter() });

global.providerWrapper = providerWrapper;
