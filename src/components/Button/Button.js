import React from "react";
import PropTypes from "prop-types";

import "./Button.css";
import { Icon } from "../index";

const exampleFunc = arg => {
  return arg;
};

const Button = props => {
  const { icon, type, onClick = exampleFunc } = props;

  return (
    <button onClick={onClick} className={icon ? "light" : type}>
      {icon && <Icon {...icon} />}
      {props.children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string
};

Button.defaultProps = {
  onClick: null,
  icon: null,
  type: null
};

export default Button;
