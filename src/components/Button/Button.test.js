import React from "react";
import { mount } from "enzyme";

import Button from "./Button";

it("renders without crashing", () => {
  const wrapper = mount(<Button>A button</Button>);
  expect(wrapper.length).toEqual(1);
});

it("simulates click events", () => {
  const mockCallBack = jest.fn();
  const wrapper = mount(<Button onClick={mockCallBack} />);

  wrapper.find("button").simulate("click");
  expect(mockCallBack.mock.calls.length).toEqual(1);
});
