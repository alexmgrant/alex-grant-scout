export { default as Button } from "./Button/Button";
export { default as Spinner } from "./Spinner/Spinner";
export { default as Icon, IconShape } from "./Icon/Icon";
export { default as Modal } from "./Modal/Modal";
export { default as Table } from "./Table/Table";
