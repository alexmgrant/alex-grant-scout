import React from "react";
import { mount } from "enzyme";

import Spinner from "./Spinner";

const wrapper = mount(<Spinner />);

it("renders without crashing", () => {
  expect(wrapper.length).toEqual(1);
});

it("renders with no children when prop visible is false", () => {
  expect(wrapper.children().length).toEqual(0);
});

it("renders child nodes when visible === true", () => {
  wrapper.setProps({ visible: true });

  expect(wrapper.children().length).toEqual(1);
});

it("should have default loading text", () => {
  expect(wrapper.prop("loadingMessage")).toBe("Loading...");
});

it("should override default loading text", () => {
  const loadingMessage = "new loading message";
  wrapper.setProps({ loadingMessage });

  expect(wrapper.prop("loadingMessage")).toBe(loadingMessage);
});
