import React from "react";
import PropTypes from "prop-types";

import "./Spinner.css";

const Spinner = props => {
  const { loadingMessage, visible = false } = props;

  if (!visible) {
    return null;
  }

  return (
    <div className="loader-wrapper">
      <div className="loader"></div>
      <p>{loadingMessage}</p>
    </div>
  );
};

Spinner.propTypes = {
  loadingMessage: PropTypes.string
};

Spinner.defaultProps = {
  loadingMessage: "Loading..."
};

export default Spinner;
