import React from "react";
import { mount } from "enzyme";

import Modal from "./Modal";

const wrapper = mount(<Modal />);

it("renders without crashing", () => {
  expect(wrapper.length).toEqual(1);
});

it("renders with no children when prop visible is false", () => {
  expect(wrapper.children().length).toEqual(0);
});

it("renders child nodes when visible === true", () => {
  wrapper.setProps({ visible: true });

  expect(wrapper.children().length).toEqual(1);
});

it("renders with title and message", () => {
  wrapper.setProps({ title: "has title", message: "has message" });

  expect(wrapper.prop("title")).toBe("has title");
  expect(wrapper.prop("message")).toBe("has message");
});
