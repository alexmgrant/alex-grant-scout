import React from "react";
import PropTypes from "prop-types";

import { Button } from "../index";
import "./Modal.css";

const closeButton = {
  icon: {
    iconName: "close",
    width: 16,
    height: 16
  }
};

const Modal = props => {
  const { title, message, onClick, visible = false } = props;

  if (!visible) {
    return null;
  }

  return (
    <div className="modal-wrapper">
      <div className="modal-card">
        <div className="modal-header">
          <h2>{title}</h2>
          <Button {...closeButton} onClick={onClick} />
        </div>
        <p>{message}</p>
      </div>
    </div>
  );
};

Modal.prototype = {
  title: PropTypes.string,
  message: PropTypes.string
};

export default Modal;
