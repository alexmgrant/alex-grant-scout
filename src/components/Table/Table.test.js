import React from "react";
import { mount } from "enzyme";

import Table from "./Table";

const wrapper = mount(<Table />);

it("renders without crashing", () => {
  expect(wrapper.length).toEqual(1);
});

it("renders with no children when prop exchangeRates is null", () => {
  expect(wrapper.children().length).toEqual(0);
});

it("renders child nodes when exchangeRates has value", () => {
  wrapper.setProps({ items: [{ CAD: 1 }] });

  expect(wrapper.children().length).toEqual(1);
});

it("renders with tableHeaders prop", () => {
  const tableHeaders = ["heading 1", "heading 2"];
  wrapper.setProps({ items: [{ CAD: 1 }], tableHeaders: tableHeaders });

  wrapper
    .find("th")
    .forEach((node, index) => expect(node.text()).toEqual(tableHeaders[index]));
});
