import React from "react";
import PropTypes from "prop-types";

import "./Table.css";

const Table = props => {
  const { items, tableHeaders = [] } = props;

  if (!items) {
    return null;
  }

  return (
    <table className="table">
      <thead>
        <tr>
          {tableHeaders.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {items.map((item, index) => (
          <tr key={index}>
            <td>{item.key}</td>
            <td>{item.value}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

Table.prototype = {
  items: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.string, PropTypes.number)
  ),
  tableHeaders: PropTypes.arrayOf(PropTypes.string)
};

export default Table;
