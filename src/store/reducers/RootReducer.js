import { combineReducers } from "redux";

import exchangeRatesReducer from "../../exchange-rates/store/reducers";
import { initialState as ExchangeRateInitState } from "../../exchange-rates/store/reducers/ExchangeRateReducer";
import { initialState as UiInitState } from "../../exchange-rates/store/reducers/UiReducer";

export const initialRootState = {
  exchangeRates: {
    api: { ...ExchangeRateInitState },
    ui: { ...UiInitState }
  }
};

const rootReducer = combineReducers({ exchangeRates: exchangeRatesReducer });

export default rootReducer;
