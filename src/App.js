import React from "react";

import ExchangeRates from "./exchange-rates/ExchangeRatesContainer";
import "./App.css";

const App = () => <ExchangeRates />;

export default App;
