# Getting started

`npm i`  
`npm start`  
`npm test`  

You may also use yarn.

## Architecture & my thoughts

I opted to use React hooks and function components. They seemed intuitive to me while learning more about React. Coming from Typescript, I'm familiar with ES6 classes, but I did enjoy the simplicity component functions offer. I do miss run-time type checking.  
I see that Flow provides type checking and that Typescript can be used, but for this test's sake, PropTypes seemed suitable.

I used container and display component architecture. I assume that as the app grows, we would want to reuse simple UI components and eventually lazy load `exchange-rates`.

I opted to have the application state managed by redux. I see that React offers a `useState` hook that might be useful for UI related state like loading. It seemed since hooks are new, the community was on the fence about when to use `useState` vs react-redux's `useSelector`.

For testing, I'm thinking covering display component state changes, async actions, and reducers would provide good overall coverage.  
Coming from Angular, the use of services is essential to App architecture. In the case of the `getExchangeRates` action, I'm not sure if it's common in React to have your HTTP/Ajax calls live in a service for more isolated testing.

Thank you for this; it was a pleasure to dive deeper in to react.  
